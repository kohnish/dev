#!/bin/sh
dnf install -y iproute iputils procps-ng dnf-utils sudo vim rpmdevtools rpm-build gcc gcc-c++ make automake imake which zsh git openssh-clients curl glib-devel gtk3-devel gettext-devel vte291-devel libconfuse-devel openssl-devel ctags systemd firefox gnome-shell

useradd -m -s /bin/zsh dev
echo 'root ALL=(ALL) ALL' > /etc/sudoers
echo '%dev ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

rm -rf /var/cache/dnf 
