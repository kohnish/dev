#!/bin/bash
if [ $# -eq 0 ]; then
    exec /bin/zsh -l
else
    exec $@
fi

