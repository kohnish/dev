##### Keybinds #####
if [[ ${TERM} == 'dumb' ]]; then
  return 1
fi
zmodload -F zsh/terminfo +b:echoti +p:terminfo
typeset -gA key_info
key_info=(
  'Control'      '\C-'
  'ControlLeft'  '\e[1;5D \e[5D \e\e[D \eOd \eOD'
  'ControlRight' '\e[1;5C \e[5C \e\e[C \eOc \eOC'
  'Escape'       '\e'
  'Meta'         '\M-'
  'Backspace'    ${terminfo[kbs]}
  'BackTab'      ${terminfo[kcbt]}
  'Left'         ${terminfo[kcub1]}
  'Down'         ${terminfo[kcud1]}
  'Right'        ${terminfo[kcuf1]}
  'Up'           ${terminfo[kcuu1]}
  'Delete'       ${terminfo[kdch1]}
  'End'          ${terminfo[kend]}
  'F1'           ${terminfo[kf1]}
  'F2'           ${terminfo[kf2]}
  'F3'           ${terminfo[kf3]}
  'F4'           ${terminfo[kf4]}
  'F5'           ${terminfo[kf5]}
  'F6'           ${terminfo[kf6]}
  'F7'           ${terminfo[kf7]}
  'F8'           ${terminfo[kf8]}
  'F9'           ${terminfo[kf9]}
  'F10'          ${terminfo[kf10]}
  'F11'          ${terminfo[kf11]}
  'F12'          ${terminfo[kf12]}
  'Home'         ${terminfo[khome]}
  'Insert'       ${terminfo[kich1]}
  'PageDown'     ${terminfo[knp]}
  'PageUp'       ${terminfo[kpp]}
)
local key
for key (${(s: :)key_info[ControlLeft]}) bindkey ${key} backward-word
for key (${(s: :)key_info[ControlRight]}) bindkey ${key} forward-word
[[ -n ${key_info[Home]} ]] && bindkey ${key_info[Home]} beginning-of-line
[[ -n ${key_info[End]} ]] && bindkey ${key_info[End]} end-of-line
[[ -n ${key_info[PageUp]} ]] && bindkey ${key_info[PageUp]} up-line-or-history
[[ -n ${key_info[PageDown]} ]] && bindkey ${key_info[PageDown]} down-line-or-history
[[ -n ${key_info[Insert]} ]] && bindkey ${key_info[Insert]} overwrite-mode
if [[ ${zdouble_dot_expand} == 'true' ]]; then
  double-dot-expand() {
    if [[ ${LBUFFER} == *.. ]]; then
      LBUFFER+='/..'
    else
      LBUFFER+='.'
    fi
  }
  zle -N double-dot-expand
  bindkey '.' double-dot-expand
fi
[[ -n ${key_info[Backspace]} ]] && bindkey ${key_info[Backspace]} backward-delete-char
[[ -n ${key_info[Delete]} ]] && bindkey ${key_info[Delete]} delete-char
[[ -n ${key_info[Left]} ]] && bindkey ${key_info[Left]} backward-char
[[ -n ${key_info[Right]} ]] && bindkey ${key_info[Right]} forward-char
bindkey ' ' magic-space
bindkey "${key_info[Control]}L" clear-screen
[[ -n ${key_info[BackTab]} ]] && bindkey ${key_info[BackTab]} reverse-menu-complete
autoload -Uz is-at-least && if ! is-at-least 5.3; then
  expand-or-complete-with-redisplay() {
    print -Pn '...'
    zle expand-or-complete
    zle redisplay
  }
  zle -N expand-or-complete-with-redisplay
  bindkey "${key_info[Control]}I" expand-or-complete-with-redisplay
fi
zle-line-init() {
  (( ${+terminfo[smkx]} )) && echoti smkx
}
zle-line-finish() {
  (( ${+terminfo[rmkx]} )) && echoti rmkx
}
zle -N zle-line-init
zle -N zle-line-finish

##### History search #####
typeset -g HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='bg=magenta,fg=white,bold'
typeset -g HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='bg=red,fg=white,bold'
typeset -g HISTORY_SUBSTRING_SEARCH_GLOBBING_FLAGS='i'
typeset -g HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=''
typeset -g HISTORY_SUBSTRING_SEARCH_FUZZY=''
typeset -g BUFFER MATCH MBEGIN MEND CURSOR
typeset -g _history_substring_search_refresh_display
typeset -g _history_substring_search_query_highlight
typeset -g _history_substring_search_result
typeset -g _history_substring_search_query
typeset -g -a _history_substring_search_query_parts
typeset -g -a _history_substring_search_raw_matches
typeset -g -i _history_substring_search_raw_match_index
typeset -g -a _history_substring_search_matches
typeset -g -i _history_substring_search_match_index
typeset -g -A _history_substring_search_unique_filter
history-substring-search-up() {
  _history-substring-search-begin
  _history-substring-search-up-history ||
  _history-substring-search-up-buffer ||
  _history-substring-search-up-search
  _history-substring-search-end
}
history-substring-search-down() {
  _history-substring-search-begin
  _history-substring-search-down-history ||
  _history-substring-search-down-buffer ||
  _history-substring-search-down-search
  _history-substring-search-end
}
zle -N history-substring-search-up
zle -N history-substring-search-down
zmodload -F zsh/parameter
if [[ $+functions[_zsh_highlight] -eq 0 ]]; then
  _zsh_highlight() {
    if [[ $KEYS == [[:print:]] ]]; then
      region_highlight=()
    fi
  }
  _zsh_highlight_bind_widgets()
  {
    zmodload zsh/zleparameter 2>/dev/null || {
      echo 'zsh-syntax-highlighting: failed loading zsh/zleparameter.' >&2
      return 1
    }
    local cur_widget
    for cur_widget in ${${(f)"$(builtin zle -la)"}:#(.*|_*|orig-*|run-help|which-command|beep|yank*)}; do
      case $widgets[$cur_widget] in
        user:$cur_widget|user:_zsh_highlight_widget_*);;
        user:*) eval "zle -N orig-$cur_widget ${widgets[$cur_widget]#*:}; \
                      _zsh_highlight_widget_$cur_widget() { builtin zle orig-$cur_widget -- \"\$@\" && _zsh_highlight }; \
                      zle -N $cur_widget _zsh_highlight_widget_$cur_widget";;
        completion:*) eval "zle -C orig-$cur_widget ${${widgets[$cur_widget]#*:}/:/ }; \
                            _zsh_highlight_widget_$cur_widget() { builtin zle orig-$cur_widget -- \"\$@\" && _zsh_highlight }; \
                            zle -N $cur_widget _zsh_highlight_widget_$cur_widget";;
        builtin) eval "_zsh_highlight_widget_$cur_widget() { builtin zle .$cur_widget -- \"\$@\" && _zsh_highlight }; \
                       zle -N $cur_widget _zsh_highlight_widget_$cur_widget";;
        *) echo "zsh-syntax-highlighting: unhandled ZLE widget '$cur_widget'" >&2 ;;
      esac
    done
  }
  _zsh_highlight_bind_widgets
fi
_history-substring-search-begin() {
  setopt localoptions extendedglob
  _history_substring_search_refresh_display=
  _history_substring_search_query_highlight=
  if [[ -n $BUFFER && $BUFFER == ${_history_substring_search_result:-} ]]; then
    return;
  fi
  _history_substring_search_result=''
  if [[ -z $BUFFER ]]; then
    _history_substring_search_query=
    _history_substring_search_query_parts=()
    _history_substring_search_raw_matches=()
  else
    _history_substring_search_query=$BUFFER
    if [[ -n $HISTORY_SUBSTRING_SEARCH_FUZZY ]]; then
      _history_substring_search_query_parts=(${=_history_substring_search_query})
    else
      _history_substring_search_query_parts=(${_history_substring_search_query})
    fi
    local search_pattern="*${(j:*:)_history_substring_search_query_parts[@]//(#m)[\][()|\\*?#~^]/\\$MATCH}*"
    _history_substring_search_raw_matches=(${(k)history[(R)(#$HISTORY_SUBSTRING_SEARCH_GLOBBING_FLAGS)${search_pattern}]})
  fi
  _history_substring_search_raw_match_index=0
  _history_substring_search_matches=()
  _history_substring_search_unique_filter=()
  if [[ $WIDGET == history-substring-search-down ]]; then
     _history_substring_search_match_index=1
  else
    _history_substring_search_match_index=0
  fi
}
_history-substring-search-end() {
  setopt localoptions extendedglob
  _history_substring_search_result=$BUFFER
  if [[ $_history_substring_search_refresh_display -eq 1 ]]; then
    region_highlight=()
    CURSOR=${#BUFFER}
  fi
  _zsh_highlight
  if [[ -n $_history_substring_search_query_highlight ]]; then
    local highlight_start_index=0
    local highlight_end_index=0
    for query_part in $_history_substring_search_query_parts; do
      local escaped_query_part=${query_part//(#m)[\][()|\\*?#~^]/\\$MATCH}
      local query_part_match_index=${${BUFFER:$highlight_start_index}[(i)(#$HISTORY_SUBSTRING_SEARCH_GLOBBING_FLAGS)${escaped_query_part}]}
      if [[ $query_part_match_index -le ${#BUFFER:$highlight_start_index} ]]; then
        highlight_start_index=$(( $highlight_start_index + $query_part_match_index ))
        highlight_end_index=$(( $highlight_start_index + ${#query_part} ))
        region_highlight+=("$(($highlight_start_index - 1)) $(($highlight_end_index - 1)) $_history_substring_search_query_highlight")
      fi
    done
  fi
  return 0
}
_history-substring-search-up-buffer() {
  local buflines XLBUFFER xlbuflines
  buflines=(${(f)BUFFER})
  XLBUFFER=$LBUFFER"x"
  xlbuflines=(${(f)XLBUFFER})
  if [[ $#buflines -gt 1 && $CURSOR -ne $#BUFFER && $#xlbuflines -ne 1 ]]; then
    zle up-line-or-history
    return 0
  fi
  return 1
}
_history-substring-search-down-buffer() {
  local buflines XRBUFFER xrbuflines
  buflines=(${(f)BUFFER})
  XRBUFFER="x"$RBUFFER
  xrbuflines=(${(f)XRBUFFER})
  if [[ $#buflines -gt 1 && $CURSOR -ne $#BUFFER && $#xrbuflines -ne 1 ]]; then
    zle down-line-or-history
    return 0
  fi
  return 1
}
_history-substring-search-up-history() {
  if [[ -z $_history_substring_search_query ]]; then
    if [[ $HISTNO -eq 1 ]]; then
      BUFFER=
    else
      zle up-line-or-history
    fi
    return 0
  fi
  return 1
}
_history-substring-search-down-history() {
  if [[ -z $_history_substring_search_query ]]; then
    if [[ $HISTNO -eq 1 && -z $BUFFER ]]; then
      BUFFER=${history[1]}
      _history_substring_search_refresh_display=1
    else
      zle down-line-or-history
    fi
    return 0
  fi
  return 1
}
_history_substring_search_process_raw_matches() {
  while [[ $_history_substring_search_raw_match_index -lt $#_history_substring_search_raw_matches ]]; do
    _history_substring_search_raw_match_index+=1
    local index=${_history_substring_search_raw_matches[$_history_substring_search_raw_match_index]}
    if [[ ! -o HIST_IGNORE_ALL_DUPS && -n $HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE ]]; then
      local entry=${history[$index]}
      if [[ -z ${_history_substring_search_unique_filter[$entry]} ]]; then
        _history_substring_search_unique_filter[$entry]=1
        _history_substring_search_matches+=($index)
        return 0
      fi
    else
      _history_substring_search_matches+=($index)
      return 0
    fi
  done
  return 1
}
_history-substring-search-has-next() {
  if  [[ $_history_substring_search_match_index -lt $#_history_substring_search_matches ]]; then
    return 0
  else
    _history_substring_search_process_raw_matches
    return $?
  fi
}
_history-substring-search-has-prev() {
  if [[ $_history_substring_search_match_index -gt 1 ]]; then
    return 0
  else
    return 1
  fi
}
_history-substring-search-found() {
  BUFFER=$history[$_history_substring_search_matches[$_history_substring_search_match_index]]
  _history_substring_search_query_highlight=$HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND
}
_history-substring-search-not-found() {
  BUFFER=$_history_substring_search_query
  _history_substring_search_query_highlight=$HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND
}
_history-substring-search-up-search() {
  _history_substring_search_refresh_display=1
  if [[ $_history_substring_search_match_index -gt $#_history_substring_search_matches ]]; then
    _history-substring-search-not-found
    return
  fi
  if _history-substring-search-has-next; then
    _history_substring_search_match_index+=1
    _history-substring-search-found
  else
    _history_substring_search_match_index+=1
    _history-substring-search-not-found
  fi
  if [[ -o HIST_IGNORE_ALL_DUPS || -n $HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE ]]; then
    return
  fi
  if [[ -o HIST_FIND_NO_DUPS && $BUFFER == $_history_substring_search_result ]]; then
    _history-substring-search-up-search
  fi
}
_history-substring-search-down-search() {
  _history_substring_search_refresh_display=1
  if [[ $_history_substring_search_match_index -lt 1 ]]; then
    _history-substring-search-not-found
    return
  fi
  if _history-substring-search-has-prev; then
    _history_substring_search_match_index+=-1
    _history-substring-search-found
  else
    _history_substring_search_match_index+=-1
    _history-substring-search-not-found
  fi
  if [[ -o HIST_IGNORE_ALL_DUPS || -n $HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE ]]; then
    return
  fi
  if [[ -o HIST_FIND_NO_DUPS && $BUFFER == $_history_substring_search_result ]]; then
    _history-substring-search-down-search
  fi
}
#zmodload -F zsh/terminfo +p:terminfo
bindkey "${terminfo[kcuu1]}" history-substring-search-up
bindkey "${terminfo[kcud1]}" history-substring-search-down

##### History options #####
HISTFILE="${ZDOTDIR:-${HOME}}/.zsh_history"
HISTSIZE=10000
SAVEHIST=10000
setopt BANG_HIST
setopt EXTENDED_HISTORY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS
setopt HIST_VERIFY

##### Directory #####
setopt AUTO_CD
setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_SILENT
setopt PUSHD_TO_HOME
setopt EXTENDED_GLOB
setopt MULTIOS
setopt NO_CLOBBER

##### Custom #####
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' formats ' (%b) '
precmd() {
vcs_info
}
PROMPT='[%n@%m %(3~|..%2~|%~)]%F{blue}${vcs_info_msg_0_}%f%(!.#.$) '
setopt correct
setopt prompt_subst
zstyle ':completion:*' rehash true
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
bindkey '^R' history-incremental-search-backward
autoload -Uz compinit
compinit
